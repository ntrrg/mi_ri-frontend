FROM golang:1.11.2-alpine3.8 AS build
RUN apk update && apk add git make
WORKDIR /src
COPY . .
RUN make

FROM alpine:3.8 as debug
COPY --from=build /src /mi_ri

FROM scratch
COPY --from=build /src/ /mi_ri
WORKDIR /mi_ri
EXPOSE 4005
USER 1000
ENTRYPOINT ["dist/mi_ri-frontend"]

