// Copyright 2018 Miguel Angel Rivera Notararigo. All rights reserved.
// This source code was released under the MIT license.

package main

import (
	"html/template"
)

var templates map[string]*template.Template

func loadTemplates() {
	templates = map[string]*template.Template{
		"default": newTemplate("base.html"),
		"error":   newTemplate("base.html", "error.html"),
		"login":   newTemplate("base.html", "login.html"),
		"recover": newTemplate("base.html", "recover.html"),
		"signup":  newTemplate("base.html", "signup.html"),
		"user":    newTemplate("base.html", "user.html"),
	}
}

func newTemplate(files ...string) *template.Template {
	for i := range files {
		files[i] = "templates/" + files[i]
	}

	return template.Must(template.New("*").ParseFiles(files...))
}
