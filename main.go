// Copyright 2018 Miguel Angel Rivera Notararigo. All rights reserved.
// This source code was released under the MIT license.

package main

import (
	"flag"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"

	nthttp "github.com/ntrrg/ntgo/net/http"
)

var (
	addr      string
	baseurl   string
	apiurl    *url.URL
	key, cert string
	logfile   string
)

func main() {
	loadTemplates()
	config()

	server := nthttp.NewServer(&nthttp.Config{
		Addr:    addr,
		Handler: Mux(),
	})

	var err error

	log.Printf("[INFO][SERVER] Listening on %v..\n", addr)
	log.Printf("[INFO][SERVER] Redirecting '%v/api/' to %v..\n", baseurl, apiurl)

	if strings.Contains(addr, "/") {
		err = server.ListenAndServeUDS()
	} else if key != "" && cert != "" {
		err = server.ListenAndServeTLS(cert, key)
	} else {
		err = server.ListenAndServe()
	}

	if err != http.ErrServerClosed {
		log.Fatalf("[FATAL][SERVER] Can't start the server -> %v", err)
	}

	<-server.Done
}

func config() {
	var backend string

	flag.StringVar(
		&addr, "addr", ":4005",
		"TCP address to listen on. If a path to a file is given, the server will "+
			"use a Unix Domain Socket.",
	)

	flag.StringVar(&baseurl, "url", "http://localhost:4005", "Base URL")
	flag.StringVar(&backend, "api", "http://localhost:4000", "RESTful API URL")
	flag.StringVar(&key, "key", "", "TLS private key file")
	flag.StringVar(&cert, "cert", "", "TLS certificate file")
	flag.StringVar(&logfile, "log", "", "Log file location (default: stderr)")
	flag.Parse()

	if logfile != "" {
		lf, err := os.OpenFile(logfile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
		if err != nil {
			log.Fatalf("[FATAL][SERVER] Can't create/open the log file -> %v", err)
		}

		defer func() {
			if err := lf.Close(); err != nil {
				log.Printf("[ERROR][SERVER] Can't close the log file -> %v", err)
			}
		}()

		log.SetOutput(lf)
	}

	var err error
	apiurl, err = url.Parse(backend)
	if err != nil {
		log.Fatal("Invalid RESTful API URL -> %v", err)
	}
}
