// Copyright 2018 Miguel Angel Rivera Notararigo. All rights reserved.
// This source code was released under the MIT license.

package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"time"
)

// SignUp manages the sign up form.
func SignUp(w http.ResponseWriter, r *http.Request) {
	_, err := r.Cookie("token")
	if err == nil {
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	switch r.Method {
	case http.MethodGet:
		render(w, "signup", baseurl)
	case http.MethodPost:
		email := r.FormValue("email")
		password := r.FormValue("password")

		if email == "" || password == "" {
			errResponse(w, "bad-data")
		}

		data, err := json.Marshal(map[string]interface{}{
			"email":    email,
			"password": password,
		})

		if err != nil {
			log.Printf("Can't encode the user data -> %v", err)
			return
		}

		urluser := baseurl + "/api/v1/users/"
		req, err := http.NewRequest(http.MethodPost, urluser, bytes.NewReader(data))
		if err != nil {
			log.Printf("Can't create the request -> %v", err)
			errResponse(w, "internal")
			return
		}

		req.Header.Set("Content-Type", "application/json; charset=utf-8")
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			log.Printf("Can't do the request -> %v", err)
			errResponse(w, "internal")
			return
		}

		defer resp.Body.Close()

		if resp.StatusCode != http.StatusCreated {
			log.Printf("Bad response from API -> %v", resp.StatusCode)
			errResponse(w, "signup")
			return
		}

		for _, cookie := range resp.Cookies() {
			w.Header().Add("Set-Cookie", cookie.String())
		}

		http.Redirect(w, r, "/", http.StatusSeeOther)
	}
}

// LogIn manages the log in form.
func LogIn(w http.ResponseWriter, r *http.Request) {
	_, err := r.Cookie("token")
	if err == nil {
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	switch r.Method {
	case "GET":
		render(w, "login", baseurl)
	case "POST":
		email := r.FormValue("email")
		password := r.FormValue("password")

		if email == "" || password == "" {
			errResponse(w, "bad-data")
		}

		urluser := baseurl + "/api/v1/token"
		req, err := http.NewRequest(http.MethodGet, urluser, nil)
		if err != nil {
			log.Printf("Can't create the request -> %v", err)
			errResponse(w, "internal")
			return
		}

		req.SetBasicAuth(email, password)
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			log.Printf("Can't do the request -> %v", err)
			errResponse(w, "internal")
			return
		}

		defer resp.Body.Close()

		code := resp.StatusCode
		switch {
		case code == http.StatusUnauthorized:
			errResponse(w, "bad-credential")
			log.Printf("Bad credential -> %v", resp.StatusCode)
			return
		case code != http.StatusOK:
			log.Printf("Bad response from API -> %v", resp.StatusCode)
			errResponse(w, "backend")
			return
		}

		for _, cookie := range resp.Cookies() {
			w.Header().Add("Set-Cookie", cookie.String())
		}

		http.Redirect(w, r, "/", http.StatusSeeOther)
	}
}

// Recover manages the recovery form.
func Recover(w http.ResponseWriter, r *http.Request) {
	_, err := r.Cookie("token")
	if err == nil {
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	switch r.Method {
	case "GET":
		render(w, "recover", nil)
	case "POST":
		email := r.FormValue("email")

		if email == "" {
			errResponse(w, "bad-data")
		}

		urluser := baseurl + "/api/v1/recover-link/?email=" + url.QueryEscape(email) + "&redirect=" + url.QueryEscape(baseurl+"/login")
		resp, err := http.Get(urluser)
		if err != nil {
			log.Printf("Can't do the request -> %v", err)
			errResponse(w, "internal")
			return
		}

		defer resp.Body.Close()

		code := resp.StatusCode
		switch {
		case code == http.StatusNotFound:
			errResponse(w, "bad-credential")
			log.Printf("Bad credential -> %v", resp.StatusCode)
			return
		case code != http.StatusOK:
			log.Printf("Bad response from API -> %v", resp.StatusCode)
			errResponse(w, "backend")
			return
		}

		render(w, "default", "The recovery link should be in your email.")
	}
}

// Users manages the users information forms.
func Users(w http.ResponseWriter, r *http.Request) {
	_, err := r.Cookie("token")
	if err != nil {
		http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
		return
	}

	if r.URL.Path != "/" {
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	switch r.Method {
	case "GET":
		GetUser(w, r)
	case "POST":
		UpdateUser(w, r)
	}
}

// GetUser fetches the user data from the REST API.
func GetUser(w http.ResponseWriter, r *http.Request) {
	tokenCookie, err := r.Cookie("token")
	if err != nil {
		http.Redirect(w, r, "/login/", http.StatusTemporaryRedirect)
		return
	}

	userurl := baseurl + "/api/v1/users/"
	req, err := http.NewRequest(http.MethodGet, userurl, nil)
	if err != nil {
		log.Printf("Can't create the request -> %v", err)
		errResponse(w, "internal")
		return
	}

	req.Header.Set("Authorization", "Bearer "+tokenCookie.Value)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Printf("Can't do the request -> %v", err)
		errResponse(w, "internal")
		return
	}

	defer resp.Body.Close()

	code := resp.StatusCode
	switch {
	case code == http.StatusUnauthorized:
		http.Redirect(w, r, "/logout/", http.StatusTemporaryRedirect)
		return
	case code != http.StatusOK:
		log.Printf("Bad response from API -> %v", resp.StatusCode)
		errResponse(w, "backend")
		return
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		errResponse(w, "internal")
		return
	}

	user := make(map[string]interface{})
	if err := json.Unmarshal(data, &user); err != nil {
		log.Printf("Can't decode the user data -> %v", err)
		errResponse(w, "internal")
		return
	}

	render(w, "user", user)
}

// UpdateUser displays a user form and sends the user data to the REST API.
func UpdateUser(w http.ResponseWriter, r *http.Request) {
	tokenCookie, err := r.Cookie("token")
	if err != nil {
		http.Redirect(w, r, "/login/", http.StatusTemporaryRedirect)
		return
	}

	email := r.FormValue("email")
	password := r.FormValue("password")
	name := r.FormValue("name")
	phone := r.FormValue("phone")
	address := r.FormValue("address")

	if email == "" {
		errResponse(w, "bad-data")
	}

	data, err := json.Marshal(map[string]interface{}{
		"email":    email,
		"password": password,
		"name":     name,
		"phone":    phone,
		"address":  address,
	})

	if err != nil {
		log.Printf("Can't encode the user data -> %v", err)
		return
	}

	url := baseurl + "/api/v1/users/"
	req, err := http.NewRequest(http.MethodPut, url, bytes.NewReader(data))
	if err != nil {
		log.Printf("Can't create the request -> %v", err)
		errResponse(w, "internal")
		return
	}

	req.Header.Set("Authorization", "Bearer "+tokenCookie.Value)
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Printf("Can't do the request -> %v", err)
		errResponse(w, "internal")
		return
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		log.Printf("Bad response from API -> %v", resp.StatusCode)
		errResponse(w, "backend")
		return
	}

	http.Redirect(w, r, "/", http.StatusSeeOther)
}

// LogOut removes the JWT cookie.
func LogOut(w http.ResponseWriter, r *http.Request) {
	exp := time.Now()

	http.SetCookie(w, &http.Cookie{
		Name:    "token",
		Value:   "",
		Path:    "/",
		Expires: exp,
	})

	http.Redirect(w, r, "/login/", http.StatusTemporaryRedirect)
}

func errResponse(w http.ResponseWriter, kind string) {
	var (
		status int
		msg    string
	)

	switch kind {
	case "bad-data":
		status = http.StatusBadRequest
		msg = "Invalid information given"
	case "bad-credential":
		status = http.StatusUnauthorized
		msg = "Bad credentials"
	case "backend":
		status = http.StatusBadGateway
		msg = "Sorry, can't get the user data, try later."
	case "signup":
		status = http.StatusConflict
		msg = "The user already exists."
	case "internal":
		fallthrough
	default:
		status = http.StatusInternalServerError
		msg = "A internal server happen, try again later."
	}

	w.WriteHeader(status)
	render(w, "error", map[string]interface{}{
		"status": status,
		"msg":    msg,
	})
}

func render(w http.ResponseWriter, name string, data interface{}) {
	t, ok := templates[name]
	if !ok {
		log.Printf("Can't find the given template -> %v", name)
		return
	}

	if err := t.ExecuteTemplate(w, "base", data); err != nil {
		log.Print(err)
	}
}
