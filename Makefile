gofiles := $(shell find . -iname "*.go" -type f)
gofiles := $(filter-out ./vendor/%, $(gofiles))
gosrcfiles := $(filter-out %_test.go, $(gofiles))
pkg_name := mi_ri-frontend
make_bin := /tmp/$(pkg_name)-bin

.PHONY: all
all: build

.PHONY: build
build: dist/$(pkg_name)

.PHONY: build-docker
build-docker:
	docker build -t ntrrg/$(pkg_name) .

.PHONY: build-docker-debug
build-docker-debug:
	docker build --target debug -t ntrrg/$(pkg_name):debug .

.PHONY: clean
clean:
	rm -rf dist/

dist/$(pkg_name): $(gosrcfiles)
	CGO_ENABLED=0 go build -o $@

# Development

.PHONY: ci
ci: lint build

.PHONY: clean-dev
clean-dev: clean
	docker image rm ntrrg/$(pkg_name) || true

.PHONY: deps-dev
deps-dev:
	go get -u -v golang.org/x/lint/golint

.PHONY: format
format:
	gofmt -s -w -l $(gofiles)

.PHONY: lint
lint:
	gofmt -d -e -s $(gofiles)
	golint ./

