// Copyright 2018 Miguel Angel Rivera Notararigo. All rights reserved.
// This source code was released under the MIT license.

package main

import (
	"net/http"
	"net/http/httputil"

	"github.com/ntrrg/ntgo/net/http/middleware"
)

// Mux creates the routes handler and bind the path with each handler.
func Mux() http.Handler {
	mux := http.NewServeMux()
	mux.HandleFunc("/signup/", SignUp)
	mux.HandleFunc("/login/", LogIn)
	mux.HandleFunc("/recover/", Recover)
	mux.HandleFunc("/", Users)
	mux.HandleFunc("/logout/", LogOut)

	mux.HandleFunc("/healthz/", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/api/healthz", http.StatusTemporaryRedirect)
	})

	mux.Handle("/api/", middleware.Adapt(
		httputil.NewSingleHostReverseProxy(apiurl),
		middleware.StripPrefix("/api"),
	))

	return mux
}
